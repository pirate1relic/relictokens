import React from 'react'
import App from '../../App'
import NavbarStyle from '../navbar/navbar.css'

const navbar = props => {

    
    return (
        <div className= "navbar">
            <div className='relicToken'>
                <p>RelicToken</p>
            </div>
            <div className ="navbar-nav">
                <div><p>Home</p></div>
                <div><p>Lore</p></div>
                <div><p>Team</p></div>
                <div className="launch">
                    <button className="button">Launch</button>
                </div>
            </div>
        </div>
    )
}

export default navbar
