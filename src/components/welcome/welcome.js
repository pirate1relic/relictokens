import React from 'react'
import App from '../../App'
import Nave from '../../img/nave.png'
import Welcome from '../welcome/welcome.css'

const welcome = () => {
    return (
        <div className="container">
            <div className="welcome">
                <div className="title">
                    <p>Welcome to <span>Relic</span> Universe</p>
                </div>
                <div className="ship">
                    <img src={Nave}></img>
                </div>
            </div>
            <div className="description">
                <div className="text-dtion">
                    <p>
                            This is the gateway to your own spaceship. The path to become the most powerfull
                        pirate in the galaxy is getting closer. be whise enought and keep both eyes open,
                        on the tricky road you could get into many risky adventures until you find Captain Inox's
                        legendary treasure.
                        
                    </p>
                </div>  
            </div>
            <div className="buttons">
                <button className="buy">Buy Relic</button>
                <button className="how">How to Buy</button>
            </div>
        </div>
    )
}

export default welcome
