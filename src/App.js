import planeta from '../src/img/planeta.png';
import './App.css';
import Navbar from './components/navbar/navbar';
import Welcome from '../src/components/welcome/welcome'
function App() {
  return (
      <div className="main" >
        <Navbar name="edward"></Navbar>
        <Welcome></Welcome>
      </div>
    );
}

export default App;
